//
//  BlackwordEntryController.m
//  Dict Shun
//
//  Created by Rikk Carey on 4/17/15.
//  Copyright (c) 2015 Dictionary.com. All rights reserved.
//

#import "BlackwordEntryController.h"
#import <OpenEars/OELanguageModelGenerator.h>
#import <OpenEars/OEAcousticModel.h>
#import <AudioToolbox/AudioToolbox.h>


@interface BlackwordEntryController ()

@end

@implementation BlackwordEntryController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Add the Cancel button
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)];
    
    self.navigationItem.leftBarButtonItem = cancelBtn;

    [self.textField setAutocorrectionType:UITextAutocorrectionTypeYes];
    
    self.errorLabel.text = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancel:(id)sender {
    //Cancel add word.
    NSLog(@"DEBUG> Cancel add word!");

    [self dismissViewControllerAnimated:YES completion:nil];
    return;
}

- (IBAction)editingDidBegin:(id)sender {
    NSLog(@"DEBUG> Text field begin edit...");
}

- (IBAction)editingChanged:(id)sender {
    NSLog(@"DEBUG> Text field editing changed...");

}

- (IBAction)didEndOnExit:(id)sender {
    // test
    NSLog(@"DEBUG> text field did end on exit...");
    
    /*
    NSString *newItem = self.textField.text;
    if (newItem != nil) {
        [self.blacklist  insertObject:newItem atIndex:[self.blacklist count]];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];}
     */
}

- (IBAction)saveWord:(id)sender {
    /*  From Apple doc:
    UITextField *textField = [(BlackwordEntryController *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] textField];
    
    SimpleEditableListAppDelegate *controller = (SimpleEditableListAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *newItem = textField.text;
    if (newItem != nil) {
        [controller insertObject:newItem inListAtIndex:[controller countOfList]];
    } */
    
    NSString *newItem = [self.textField.text uppercaseString];
    
    // Check if its a valid word: a) no spaces, b) 
    for (int i = 0; i < [self.words count]; i++) {
        if ([newItem isEqualToString:self.words[i]]) {
            self.errorLabel.text = @"This word already exists in your list.";
            AudioServicesPlaySystemSound(1073); // iOS error sound
            self.textField.text = @"";
            return;
        }
    }
    
    if (newItem != nil) {
        [self.words insertObject:newItem atIndex:[self.words count]];
        [self.wordCount insertObject:[NSNumber numberWithInt:0] atIndex:[self.wordCount count]];
    }
    
    self.textField.text = @"";
    
    // Save to UserDefaults
    [self saveBlacklist];
    
    // Regenerate OE vocabulary.
    NSError *err = [self.lmGenerator generateLanguageModelFromArray:_words withFilesNamed:@"MyLanguageModelFiles" forAcousticModelAtPath:[OEAcousticModel pathToModel:@"AcousticModelEnglish"]];

    if (err == nil) {
        NSLog(@"DEBUG> Language model SUCCESSFULLY generated, for keywords: %@!", _words);
    } else {
        NSLog(@"DEBUG> Error generating lanaguage model for keywords list: %@",[err localizedDescription]);
    }
    
    // Dismiss this modal. <-- probably should be delegated to parent view (presenting view).
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) saveBlacklist
{
    // Save to User Defaults (local).
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSData *blacklistData = [NSKeyedArchiver archivedDataWithRootObject:self.words];
    [defaults setObject:blacklistData forKey:@"Bleeper-blacklist-words"];
    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    
    [defaults setObject:version forKey:@"Bleeper-version"];
    [defaults setObject:build forKey:@"Bleeper-build"];
    
    [defaults synchronize];
    
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
