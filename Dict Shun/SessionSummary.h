//
//  SessionSummary.h
//  Bleeper
//
//  Created by Rikk Carey on 4/28/15.
//  Copyright (c) 2015 Dictionary.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma WordSummary

@interface WordSummary : NSObject <NSCoding> {
    
}

@property (nonatomic)       NSString  * word;
@property (nonatomic)       int         count;

- (void)encodeWithCoder:(NSCoder *)enCoder;
- (id)initWithCoder:(NSCoder *)deCoder;

@end


#pragma SessionSummary

@interface SessionSummary : NSObject <NSCoding> {

}

@property (nonatomic)       NSDate          * date;
@property (nonatomic)       float             duration;
@property (nonatomic)       NSMutableArray  * sessionInfo;  // array of WordSummary(ies)

- (void)encodeWithCoder:(NSCoder *)enCoder;
- (id)initWithCoder:(NSCoder *)deCoder;

@end
