//
//  HistoryController.h
//  Bleeper
//
//  Created by Rikk Carey on 4/28/15.
//  Copyright (c) 2015 Dictionary.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryController : UIViewController
@property (nonatomic, strong) NSMutableArray            * sessionHistory;
@property (weak, nonatomic) IBOutlet UIButton *clearHistoryButton;
@property (weak, nonatomic) IBOutlet UITableView        * tableView;
@end
