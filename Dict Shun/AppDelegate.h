//
//  AppDelegate.h
//  Dict Shun
//
//  Created by Rikk Carey on 3/31/15.
//  Copyright (c) 2015 Dictionary.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <OpenEars/OELanguageModelGenerator.h>


#pragma AppDelegate

#define SETTING_DURATION_DEFAULT             0  // Index of segmented control
#define SETTING_VAD_THRESHOLD_DEFAULT        1  // Index of segmented control
#define SETTING_SECONDS_OF_SILENCE_DEFAULT   0  // Index of segmented control


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow          *   window;

// Open Ears speech recognition
@property (strong, nonatomic) OELanguageModelGenerator * lmGenerator;

// Data model
@property (strong, nonatomic)   NSMutableArray    *     words;
@property (strong, nonatomic)   NSMutableArray    *     wordCount;
@property (strong, nonatomic)   NSMutableArray    *     sessionHistory;

@property (nonatomic)           int                     VADThresholdSetting;        // segmented control
@property (readonly, nonatomic) float                   VADThreshold;               // readonly, use settings

@property (nonatomic)           int                     secondsOfSilenceSetting;    //segmented control
@property (readonly, nonatomic) float                   secondsOfSilence;           // readonly, use settings

@property (nonatomic)           int                     sessionDurationSetting;     // segmented control
@property (readonly, nonatomic) float                   sessionDuration;            // readonly, use settings

@property (nonatomic)           BOOL                    rapidEarsEnabled;           // switch control

// App version
@property (strong, nonatomic) NSString          *   version;
@property (strong, nonatomic) NSString          *   build;

@end

