//
//  BlackwordEntryController.h
//  Dict Shun
//
//  Created by Rikk Carey on 4/17/15.
//  Copyright (c) 2015 Dictionary.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <OpenEars/OELanguageModelGenerator.h>


@interface BlackwordEntryController : UIViewController <UITextFieldDelegate>

// Open Ears speech recognition
@property (strong, nonatomic) OELanguageModelGenerator  * lmGenerator;

// Views
@property (weak, nonatomic) IBOutlet    UILabel         * errorLabel;
@property (weak, nonatomic) IBOutlet    UITextField     * textField;

// Data model
@property (weak, nonatomic)             NSMutableArray  * words;
@property (weak, nonatomic)             NSMutableArray  * wordCount;

@end
