//
//  SessionOverController.h
//  Bleeper
//
//  Created by Rikk Carey on 4/22/15.
//  Copyright (c) 2015 Dictionary.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SessionOverDelegate;

@interface SessionOverController : UIViewController
@property (strong, nonatomic)               NSMutableArray  *   wordCount;
@property (strong, nonatomic)               NSMutableArray  *   words;
@property (weak, nonatomic) IBOutlet        UITableView     *   tableView;
@property (nonatomic, weak) id<SessionOverDelegate>             delegate;
@end

@protocol SessionOverDelegate
- (void)closeSessionOver:(SessionOverController *)sessionOverController;

@end
