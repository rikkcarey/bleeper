//
//  SessionSummary.m
//  Bleeper
//
//  Created by Rikk Carey on 4/28/15.
//  Copyright (c) 2015 Dictionary.com. All rights reserved.
//

#import "SessionSummary.h"

#pragma WordSummary
@implementation WordSummary

- (id) init {
    if ((self = [super init]) != nil) {
        self->_word = nil;
        self->_count = 0;
    }
    
    return self;
}

- (void) encodeWithCoder:(NSCoder *)enCoder {
    //[super encodeWithCoder:enCoder];  // Needed if superclass supports NSCoding
    
    [enCoder encodeObject:self.word forKey:@"Session-word-word"];
    [enCoder encodeInt:self.count forKey:@"Session-word-count"];
}

- (id) initWithCoder:(NSCoder *)deCoder {
    if (self = [super init]) {
        self->_word     = [deCoder decodeObjectForKey:@"Session-word-word"];
        self->_count    = [deCoder decodeIntForKey:@"Session-word-count"];

    }
    
    return self;
}
@end


#pragma SessionSummary

@implementation SessionSummary

+ (SessionSummary *) session
{
    return [[self alloc] init];
}

- (id) init {
    // Use self->_ for all properties in init methods:
    // a. Properties may not be fully created yet in the init method (standard Obj-C convention).
    // b. CCNodes added via addChild are automatically retained/released by its parent.
    
    if ((self = [super init]) != nil) {
        self->_date = nil;
        self->_duration = 0.0f;
        self->_sessionInfo = [NSMutableArray arrayWithCapacity:0];
    }
    
    return self;
}

- (void) encodeWithCoder:(NSCoder *)enCoder {
    
    //[super encodeWithCoder:enCoder];  // if superclass support NSCoding
    
    [enCoder encodeObject:self.date forKey:@"Session-date"];
    [enCoder encodeFloat:self.duration forKey:@"Session-duration"];
    [enCoder encodeObject:self.sessionInfo forKey:@"Session-info"];
}

- (id) initWithCoder:(NSCoder *)deCoder {
    
    // IMPORTANT: Keep this current with all fields defined in UserSeason (in UserModel.h)!
    if (self = [super init]) {
        self->_date         = [deCoder decodeObjectForKey:@"Session-date"];
        self->_duration     = [deCoder decodeFloatForKey:@"Session-duration"];
        self->_sessionInfo  = [deCoder decodeObjectForKey:@"Session-info"];   // +1 retain
    }
    
    return self;
}

@end
