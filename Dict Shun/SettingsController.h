//
//  SettingsController.h
//  Bleeper
//
//  Created by Rikk Carey on 4/29/15.
//  Copyright (c) 2015 Dictionary.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsController : UITableViewController
@property (weak, nonatomic) IBOutlet UISegmentedControl *sessionDurationControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *VADThresholdControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *secondsOfSilenceControl;
@property (weak, nonatomic) IBOutlet UIButton           *resetSpeechRecognitionSettings;
@property (weak, nonatomic) IBOutlet UISwitch *rapidEarsSwitch;

@end
