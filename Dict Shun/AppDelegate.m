//
//  AppDelegate.m
//  Dict Shun
//
//  Created by Rikk Carey on 3/31/15.
//  Copyright (c) 2015 Dictionary.com. All rights reserved.
//

#import "AppDelegate.h"
#import "BlacklistController.h"
#import "ListenController.h"
#import "HistoryController.h"
#import <OpenEars/OELanguageModelGenerator.h>
#import <OpenEars/OEAcousticModel.h>

#pragma AppDelegate

@interface AppDelegate ()

@end

@implementation AppDelegate

NSMutableArray *_words;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // Initialize.
    _words = [NSMutableArray arrayWithCapacity:0];
    _wordCount = [NSMutableArray arrayWithCapacity:0];
    _sessionHistory = [NSMutableArray arrayWithCapacity:0];
    
    self.VADThresholdSetting = SETTING_VAD_THRESHOLD_DEFAULT;
    self.secondsOfSilenceSetting = SETTING_SECONDS_OF_SILENCE_DEFAULT;
    self.sessionDurationSetting = SETTING_DURATION_DEFAULT;
    self.rapidEarsEnabled = NO;
    
    // Load saved values from UserDefaults.
    [self loadUserDefaults];

    // Init speech recognition vocab.
    OELanguageModelGenerator *lmGenerator = [[OELanguageModelGenerator alloc] init];
    
    NSString *name = @"MyLanguageModelFiles";
    NSError *err = [lmGenerator generateLanguageModelFromArray:_words withFilesNamed:name forAcousticModelAtPath:[OEAcousticModel pathToModel:@"AcousticModelEnglish"]];
    NSString *lmPath = nil;
    NSString *dicPath = nil;
    
    if (err == nil) {
        NSLog(@"DEBUG> Language model SUCCESSFULLY generated, for keywords: %@!", _words);

        lmPath = [lmGenerator pathToSuccessfullyGeneratedLanguageModelWithRequestedName:@"MyLanguageModelFiles"];
        dicPath = [lmGenerator pathToSuccessfullyGeneratedDictionaryWithRequestedName:@"MyLanguageModelFiles"];
        
        NSLog(@"DEBUG> Language model: path name to lang model file: %@!", lmPath);
        NSLog(@"DEBUG> Language model: path name to lang model dict: %@!", dicPath);
    } else {
        NSLog(@"DEBUG> Error generating lanaguage model for keywords list: %@",[err localizedDescription]);
    }
    
    // Init controls & views
    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    
    // 0 = settings
    // 1 = history
    // 2 = black list
    // 3 = listen
    
    // Settings control
    UINavigationController * navigationController = [tabBarController viewControllers][0];
    //SettingsController *settingsController = [navigationController viewControllers][0];
    //settingsController.xxx = _xxx;
    
    // History control
    navigationController = [tabBarController viewControllers][1];
    HistoryController *historyController = [navigationController viewControllers][0];
    historyController.sessionHistory = _sessionHistory;
    
    // Black list editor
    navigationController = [tabBarController viewControllers][2];
    BlacklistController *blacklistController = [navigationController viewControllers][0];
    blacklistController.words = _words;
    blacklistController.wordCount = _wordCount;
    blacklistController.sessionHistory = _sessionHistory;
    blacklistController.lmGenerator = _lmGenerator;

    // Listen session control
    navigationController = [tabBarController viewControllers][3];
    ListenController *listenController = [navigationController viewControllers][0];
    listenController.words = _words;
    listenController.wordCount = _wordCount;
    listenController.sessionHistory = _sessionHistory;
    listenController.lmPath = lmPath;
    listenController.dicPath = dicPath;

    return YES;
}

-(void) setSessionDurationSetting:(int)sessionDurationSetting {
    
    _sessionDurationSetting = sessionDurationSetting;
    
    if (sessionDurationSetting == 0) {
        _sessionDuration = 15.0f;
    } else if (sessionDurationSetting == 1) {
        _sessionDuration = 30.0f;
    } else if (sessionDurationSetting == 2) {
        _sessionDuration = 60.0f;
    } else if (sessionDurationSetting == 3) {
        _sessionDuration = 120.0f;
    } else _sessionDuration =  15.0f;
    
    NSLog(@"DEBUG> Session duration setting set to: %d, value set to: %f", _sessionDurationSetting, _sessionDuration);
}

-(void) setVADThresholdSetting:(int)VADThresholdSetting {
    // VADThreshold default=1.5, range: 0.5-5.0, max=3.0  Recommended by Halle of OpenEars project

    _VADThresholdSetting = VADThresholdSetting;
    
    if (VADThresholdSetting == 0) {
        _VADThreshold = 0.5f;
    } else if (_VADThresholdSetting == 1) {
        _VADThreshold = 1.5f;
    } else if (_VADThresholdSetting == 2) {
        _VADThreshold = 3.0f;
    } else _VADThreshold = 1.5f;
    
    NSLog(@"DEBUG> VADThreshold setting set to: %d, value set to: %f", _VADThresholdSetting, _VADThreshold);
}

-(void) setSecondsOfSilenceSetting:(int)secondsOfSilence {
    
    _secondsOfSilenceSetting = secondsOfSilence;
    
    if (secondsOfSilence == 0) {
        _secondsOfSilence = 0.3f;
    } else if (secondsOfSilence == 1) {
        _secondsOfSilence = 0.5f;
    } else if (secondsOfSilence == 2) {
        _secondsOfSilence = 0.7f;
    } else if (secondsOfSilence == 3) {
        _secondsOfSilence = 0.9f;
    }
    
    NSLog(@"DEBUG> SecondsOfSilence setting set to: %d, value set to: %f", _secondsOfSilenceSetting, _secondsOfSilence);
}

-(void) loadUserDefaults {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData         *data;
    
    // Load app version and build (UserDefaults).
    NSString * curVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * curBuild = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    NSLog(@"DEBUG> Current app version: %@, build: %@", curVersion, curBuild);
    
    // Load version/build from local store (UserDefaults).
    self.version = [defaults objectForKey:@"Bleeper-version"];
    self.build = [defaults objectForKey:@"Bleeper-build"];
    NSLog(@"DEBUG> App version from UserDefaults: %@, build: %@", self.version, self.build);

    // Load/init settings.
    
    // Load session duration from local store (UserDefaults).
    int durSetting = [defaults floatForKey:@"Bleeper-setting-session-duration"];        // returns 0 if no value found
    if (durSetting == 0) {
        NSLog(@"DEBUG> No session history found in local store (UserDefaults)... using default value.");
        durSetting = SETTING_DURATION_DEFAULT;
    } else NSLog(@"DEBUG> Session duration setting LOADED from User Defaults: %d", durSetting);
    self.sessionDurationSetting = durSetting;
    
    // Load VAD Threshold setting from local store (UserDefaults).
    int vadSetting = [defaults floatForKey:@"Bleeper-setting-VADThreshold"];            // returns 0 if no value found
    if (vadSetting == 0) {
        NSLog(@"DEBUG> No VAD Threshold found in local store (UserDefaults)... using default value.");
        vadSetting = SETTING_VAD_THRESHOLD_DEFAULT;
    } else NSLog(@"DEBUG> VAD Threshold LOADED from User Defaults: %d", vadSetting);
    self.VADThresholdSetting = vadSetting;
    
    // Load Seconds of Silence setting from local store (UserDefaults).
    int silenceSetting = [defaults floatForKey:@"Bleeper-setting-secondOfSilence"];     // returns 0 if no value found
    if (silenceSetting == 0) {
        NSLog(@"DEBUG> No Seconds of Silence found in local store (UserDefaults)... using default value.");
        silenceSetting = SETTING_SECONDS_OF_SILENCE_DEFAULT;
    } else NSLog(@"DEBUG> Seconds of Silence LOADED from local store: %d", silenceSetting);
    self.secondsOfSilenceSetting = silenceSetting;
    
    // RapidEars
    BOOL rapidEars = [defaults boolForKey:@"Bleeper-setting-rapidEars"];     // returns 0 if no value found
    self.rapidEarsEnabled = rapidEars;
    
    // Load blacklist from local store (UserDefaults).
    NSLog(@"DEBUG> Begin loading blacklist from local store (UserDefaults)...");
    data = [defaults objectForKey:@"Bleeper-blacklist-words"];
    if (data) {
        [self.words addObjectsFromArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
        NSLog(@"DEBUG> Blacklist LOADED from local store!");
    } else {
        // No blacklist found in UserDefaults (this should be a first time user).
        NSLog(@"DEBUG> No blacklist found in local store (UserDefaults)... INITIALIZING blacklist default... ***THIS SHOULD BE A FIRST-TIME USER!***");
        [_words addObject:@"TEST"];
        [_words addObject:@"BUMMER"];
        [_words addObject:@"DARN"];
        [_words addObject:@"TESTING"];
        [_words addObject:@"DAMN"];
        [_words addObject:@"SHUCKS"];
    }

    // Init word count model.
    for (int i = 0; i < [self.words count]; i++) {
        [self.wordCount addObject:[NSNumber numberWithInt:0]];
    }
    
    // Load session history from local store (UserDefaults).
    data = [defaults objectForKey:@"Bleeper-history"];
    if (data) {
        [self.sessionHistory addObjectsFromArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
        NSLog(@"DEBUG> Session history LOADED from local store!");
    } else {
        // No history found in UserDefaults (this should be a first time user).
        NSLog(@"DEBUG> No session history found in local store (UserDefaults). *** THIS SHOULD BE A FIRST TIME USER ***");
    }
    
    return;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
