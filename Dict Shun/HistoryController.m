//
//  HistoryController.m
//  Bleeper
//
//  Created by Rikk Carey on 4/28/15.
//  Copyright (c) 2015 Dictionary.com. All rights reserved.
//

#import "HistoryController.h"
#import "SessionSummary.h"

@interface HistoryController ()

@end

@implementation HistoryController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIBarButtonItem *eraseBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(clearHistoryButtonTapped:)];
    
    self.navigationItem.rightBarButtonItem = eraseBtn;
}

-(void) viewDidAppear:(BOOL)animated {
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
     
     //NSLog(@"DEBUG> Session history, # rows: %d", (unsigned int)[self.sessionHistory count]);
    
    return [self.sessionHistory count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Configure the cell...
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SessionSummaryCell"];
    
    // Reverse row order (recent first)
    unsigned long sessionIndex = [self.sessionHistory count] - 1 - indexPath.row;
    
    // Find # of words detected during session.
    unsigned long wordCount = 0;
    NSArray *sessionWords = ((SessionSummary *)self.sessionHistory[sessionIndex]).sessionInfo;
    for (int i = 0; i < [sessionWords count]; i++) {
        if (((WordSummary *) (sessionWords[i])).count > 0) wordCount += ((WordSummary *) (sessionWords[i])).count;
    }
    
    // Format the session date.
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    NSString *formattedDateString = [dateFormatter stringFromDate:((SessionSummary *)(self.sessionHistory[sessionIndex])).date];
    NSString *durationInSeconds = [NSString stringWithFormat:@"%.f", ((SessionSummary *)(self.sessionHistory[sessionIndex])).duration];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@", formattedDateString];
    
    if (wordCount == 0) cell.detailTextLabel.text = [NSString stringWithFormat:@"😇 Perfect! (%@ secs)", durationInSeconds];
    else cell.detailTextLabel.text = [NSString stringWithFormat:@"%zd 😈 (%@ secs)", wordCount, durationInSeconds];
    
    //NSLog(@"DEBUG> Session history tableView cellForRowAtIndexPath[%zd] = %@, %lu (data[%lu]", indexPath.row, formattedDateString, wordCount, sessionIndex);
    
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (IBAction)clearHistoryButtonTapped:(id)sender {
    NSLog(@"DEBUG> Clear history button tapped!");
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"Are you sure you want to erase your history?"
                                  delegate:(id)self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:@"Erase history"
                                  otherButtonTitles:nil];
    
    [actionSheet showInView:self.view];
    
    return;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0){
        // Confirmed.
        NSLog(@"DEBUG> Delete confirmed!");
        
        // Clear all history.
        [self.sessionHistory removeAllObjects];
        
        // Reload table.
        [self.tableView reloadData];
    } else if(buttonIndex == 1){
        // Cancelled.
        NSLog(@"DEBUG> Cancel tapped!");
    }
    
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
