//
//  ListenController.m
//  Dict Shun
//
//  Created by Rikk Carey on 3/31/15.
//  Copyright (c) 2015 Dictionary.com. All rights reserved.
//

#import "ListenController.h"
#import "AppDelegate.h"
#import "SessionSummary.h"
#import <AudioToolbox/AudioToolbox.h>
#import <OpenEars/OEPocketsphinxController.h>
#import <RapidEarsDemo/OEPocketsphinxController+RapidEars.h>
#import <OpenEars/OEAcousticModel.h>
#import <OpenEars/OELogging.h>


#define LISTEN_COUNTDOWN    3   // Countdown in seconds

#define LISTEN_READY        0   // Listen button ready for tap
#define LISTEN_PENDING      1   // Countdown in progress (no listening yet)
#define LISTEN_REQUESTED    2   // Request has been made to start listening
#define LISTEN_LISTENING    3   // Listening
#define LISTEN_STOPPING     4   // Stop listening in progress

#define BKG_WORD_DETECTED       [UIColor colorWithRed:(225/255.0) green:( 50/255.0) blue:(  0/255)   alpha:1.0]
#define BKG_LISTENING           [UIColor colorWithRed:( 50/255.0) green:(225/255.0) blue:(  0/255)   alpha:1.0]
#define BKG_COUNTDOWN           [UIColor colorWithRed:(215/255.0) green:(205/255.0) blue:(  0/255)   alpha:1.0]
#define BKG_NORMAL              [UIColor colorWithRed:( 50/255.0) green:(100/255.0) blue:(225/255.0) alpha:1.0]


@interface ListenController ()
@end

@implementation ListenController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    // Init timer.
    self.listenState = LISTEN_READY;
    self.listenStatusLabel.text = @"Tap to begin";
    self.listenButtonLabel.text = @"Start";
    self.listenScoreLabel.text = @"";
    
    // Initialize (once) OE Observer
    if (self.openEarsEventsObserver == nil) {
        self.openEarsEventsObserver = [[OEEventsObserver alloc] init];
        [self.openEarsEventsObserver setDelegate:self];
    }
    
    // Activate OE + request mic access
    [[OEPocketsphinxController sharedInstance] setActive:TRUE error:nil];
    [[OEPocketsphinxController sharedInstance] requestMicPermission];

    // Debug logs
    [OELogging startOpenEarsLogging];
    //[OEPocketsphinxController sharedInstance].verbosePocketSphinx = YES;   // <--- turn this on when debugging speech recog details
    
    // Init button behavior.
    [self.listenButton setBackgroundImage:[UIImage imageNamed:@"listen-button-selected.png"] forState:(UIControlStateDisabled|UIControlStateHighlighted|UIControlStateSelected)];
    
    return;
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    self.duration = appDelegate.sessionDuration + LISTEN_COUNTDOWN; // ??? This needs to be read from UserDefaults. ???

    int minutes = (int) ((self.duration - LISTEN_COUNTDOWN) / 60);
    int seconds = (int) ((self.duration - LISTEN_COUNTDOWN) - (minutes * 60));
    
    self.listenTimerLabel.text = [NSString stringWithFormat:@"for %u minutes and %u seconds", minutes, seconds];
    
}

- (void)updateTimer {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    // Runs every ~0.1 seconds.
    NSTimeInterval currentTime = [NSDate timeIntervalSinceReferenceDate];
    NSTimeInterval elapsedTime = currentTime - self.startTime;
    
    //NSLog(@"DEBUG> Current time: %f, Elapsed time: %f", (double) currentTime, (double) elapsedTime);

    int minutes = (int) (elapsedTime / 60);
    int seconds = (int) (elapsedTime - (minutes * 60));
    
    switch (self.listenState) {
           
        case LISTEN_READY:
        {
            // Ready state.
            NSLog(@"DEBUG> ERROR: updateTimer called while in READY state! elapsed time = %f", elapsedTime);
            NSAssert(NO, @"DEBUG> ListenController::udpateTimer: invoked but state is LISTEN_READY!");
        }
        break;

        case LISTEN_PENDING:
        {
            if (elapsedTime < ((float)LISTEN_COUNTDOWN + 1.001)) {
                // Countdown still in progress.
                //NSLog(@"DEBUG> Counting IN PROGRESS, minutes = %d, seconds = %d, elapsed time = %f", minutes, seconds, elapsedTime);
                self.listenStatusLabel.text = @"Get ready...";
                self.listenButtonLabel.font = [UIFont systemFontOfSize:60];
                self.listenButtonLabel.text = [NSString stringWithFormat:@"%d", LISTEN_COUNTDOWN - seconds];
                self.listenTimerLabel.text = @"";
                
                if (elapsedTime > self.countdownCounter) {
                    if (self.countdownCounter == LISTEN_COUNTDOWN) {
                        AudioServicesPlaySystemSound(1106);     // iOS beep-beep effect http://iphonedevwiki.net/index.php/AudioServices
                    } else {
                        AudioServicesPlaySystemSound(1113);     // 1113 = Apple System Sound "start recording"  (unclear if this is legal)  ;-)  ???
                    }
                    ++self.countdownCounter;
                }
                
                //NSLog(@"DEBUG> Countdown counter: %d, elapsed time: %f", self.countdownCounter, elapsedTime);

                [[self view] setBackgroundColor:BKG_COUNTDOWN];
            } else {
                // Countdown is done ==> Request OE to START LISTENING...
                NSLog(@"DEBUG> Countdown COMPLETE, minutes = %d, seconds = %d, elapsed time = %f", minutes, seconds, elapsedTime);
                
                self.listenStatusLabel.text = @"Begin listening...";
                self.listenButtonLabel.text = @"?";
                self.listenButtonLabel.font = [UIFont systemFontOfSize:80];
                self.listenScore = 0;
                self.listenScoreLabel.text = [NSString stringWithFormat:@"Score: %ld", (long) self.listenScore];
                self.alertTimer = -1;   // disabled
                
                //[self.listenButton setSelected:YES];
                //[self.listenButton setHighlighted:YES];

                // Init timer label
                minutes = (int) ((elapsedTime-LISTEN_COUNTDOWN) / 60);
                seconds = (int) ((elapsedTime-LISTEN_COUNTDOWN) - (minutes * 60));
                self.listenTimerLabel.text = [NSString stringWithFormat:@"%u:%02u", minutes, seconds];
                
                [[self view] setBackgroundColor:BKG_LISTENING];
                
                // Request OE to start listening...
                NSLog(@"DEBUG> Countdown COMPLETE ==> REQUEST to start listening... We are waiting for OE to confirm that listening has started.");
                
                [self.listenActivityIndicator startAnimating];  // Enable busy indicator until OE confirms.
                
                // Start up OpenEars (or RapidEars).
                // Two methods for listening: 1) Standard Open Ears, 2) Open Ears + Rapid Ears.
                
                [[OEPocketsphinxController sharedInstance] setActive:TRUE error:nil];
                [[OEPocketsphinxController sharedInstance] setSecondsOfSilenceToDetect:appDelegate.secondsOfSilence];
                [[OEPocketsphinxController sharedInstance] setVadThreshold:appDelegate.VADThreshold];
                
                NSLog(@"DEBUG> Setting OpenEars params: VAD: %f, Silence: %f", appDelegate.VADThreshold, appDelegate.secondsOfSilence);

                if (appDelegate.rapidEarsEnabled) {
                    // RapidEars is a plugin extension to OpenEars for fast, continuous speech recognition.
                    // (see RapidEarsDemo.framework/Headers/OEPocketsphinxController+RapidEars.h for details)
                    NSLog(@"DEBUG> Starting listening with OpenEars+RapidEars...");

                    [[OEPocketsphinxController sharedInstance] setRapidEarsToVerbose:TRUE];         // *** RapidEars ONLY ***  Verbose logging.
                    [[OEPocketsphinxController sharedInstance] setFinalizeHypothesis:FALSE];        // *** RapidEars ONLY ***  Disables final hypothesis. Comment out if no RapidEars.
                    [[OEPocketsphinxController sharedInstance] setReturnDuplicatePartials:FALSE];    // *** RapidEars ONLY ***  Allows duplicates (I think :).
                    
                    // "Normal" RapidEars features. Not compatible with the experimental features!
                    
                    /** Setting this to true will cause you to receive your hypotheses as separate words rather than a single NSString. This is a requirement for using OEEventsObserver delegate methods that contain timing or per-word scoring. This can't be used with N-best.*/
                    //[[OEPocketsphinxController sharedInstance] setReturnSegments:TRUE];
                    
                    /** Setting this to true will cause you to receive segment hypotheses with timing attached. This is a requirement for using OEEventsObserver delegate methods that contain word timing information. It only works if you have setReturnSegments set to TRUE. This can't be used with N-best.*/
                    //[[OEPocketsphinxController sharedInstance] setReturnSegmentTimes:TRUE];
                    
                    /** This can take a value between 1 and 4. 4 means the lowest-latency for partial hypotheses and 1 means the highest. The lower the latency, the higher the CPU overhead, and vice versa. This defaults to 4 (the lowest latency and highest CPU overhead) so you will reduce it if you have a need for less CPU overhead until you find the ideal balance between CPU overhead and speed of hypothesis.*/
                    //[[OEPocketsphinxController sharedInstance] setLatencyTuning:4];
                    
                    // "Experimental" RapidEars features. This may be worth experimenting with, but OpenEars folks say it could slow performance.
                    
                    /** EXPERIMENTAL. This will give you N-Best results, most likely at the expense of performance. This can't be used with setReturnSegments: or setReturnSegmentTimes: . This is a wholly experimental feature and can't be used with overly-large language models or on very slow devices. It is the sole responsibility of the developer to test whether performance is acceptable with this feature and to reduce language model size and latencyTuning in order to get a good UX – there is absolutely no guarantee given that this feature will not result in searches which are too slow to return if it has too much to do. */
                    //[[OEPocketsphinxController sharedInstance] setRapidEarsReturnNBest:FALSE];      // *** RapidEars ONLY ***  Experimental feature, not recommended by Halle.
                    
                    /** This is the maximum number of nbest results you want to receive. You may receive fewer than this number but will not receive more. This defaults to 3 for RapidEars; larger numbers are likely to use more CPU and smaller numbers less. Settings below 2 are invalid and will be set to 2. It is not recommended to ever set this above 3 for realtime processing. */
                    //[[OEPocketsphinxController sharedInstance] setRapidEarsNBestNumber:3];          // *** RapidEars ONLY ***  Experimental feature, not recommended by Halle.
                    
                    // Start listening with RapidEars.
                    [[OEPocketsphinxController sharedInstance] startRealtimeListeningWithLanguageModelAtPath:self.lmPath dictionaryAtPath:self.dicPath acousticModelAtPath:[OEAcousticModel pathToModel:@"AcousticModelEnglish"]];
                } else {
                    // Start listening with standard OpenEars.
                    NSLog(@"DEBUG> Starting listening with OpenEars...");
                    [[OEPocketsphinxController sharedInstance] startListeningWithLanguageModelAtPath:self.lmPath
                                                                                    dictionaryAtPath:self.dicPath
                                                                                 acousticModelAtPath:[OEAcousticModel pathToModel:@"AcousticModelEnglish"]
                                                                                 languageModelIsJSGF:NO];
                }
                self.listenState = LISTEN_REQUESTED;
            }
        }
        break;
            
        case LISTEN_REQUESTED:
        {
            // User has tapped Listen button. We are waiting for OE to confirm that listening has actually started.
            // Do nothing.
            NSLog(@"DEBUG> LISTEN REQUEST is still PENDING... waiting for OE to confirm start...");
        }
        break;

        case LISTEN_LISTENING:
        {
            if (elapsedTime > self.duration + 0.01f) {
                // Session done ==> STOP LISTENING!
                NSLog(@"DEBUG> Session duration over ==> STOP LISTENING!");
                [self.timer invalidate];               // Reset timer

                // Request OE to stop listening (wait for confirmation).
                [self.listenActivityIndicator startAnimating];
                NSError *err = [[OEPocketsphinxController sharedInstance] stopListening];
                
                if (err == nil) { NSLog(@"DEBUG> Pocketxphinx stopListening = SUCCESSFUL!"); }
                else {  NSLog(@"DEBUG> Pocketsphinx stopListening = ERROR: %@", [err localizedDescription]); }
                
                // Update views.
                minutes = (int) ((elapsedTime-LISTEN_COUNTDOWN) / 60);
                seconds = (int) ((elapsedTime-LISTEN_COUNTDOWN) - (minutes * 60));
                
                self.listenStatusLabel.text = @"Tap to begin";
                self.listenButtonLabel.text = @"Start";
                self.listenButtonLabel.font = [UIFont systemFontOfSize:24];
                self.listenTimerLabel.text = @"";

                [[self view] setBackgroundColor:BKG_NORMAL];

                self.listenScoreLabel.text = @"";
                
                // Change state to STOPPING
                self.listenState = LISTEN_STOPPING;
                
                // Display session over modal.
                if (self.sessionOverController == nil) {
                    UIStoryboard *storyboard = self.storyboard;
                    self.sessionOverController = [storyboard instantiateViewControllerWithIdentifier:@"SessionOverView"];
                    self.sessionOverController.words = self.words;
                    self.sessionOverController.wordCount = self.wordCount;
                    self.sessionOverController.delegate = self;             // Assign self as delegate.
                }
                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.sessionOverController];
                [[self navigationController] presentViewController:navigationController animated:YES completion:nil];
                
                return;     // <------ BREAK!
                
            } else {
                // CONTINUE listening...
                minutes = (int) ((elapsedTime-LISTEN_COUNTDOWN) / 60);
                seconds = (int) ((elapsedTime-LISTEN_COUNTDOWN) - (minutes * 60));
                
                if (self.alertTimer == 0) {
                    // Word alert done.
                    //NSLog(@"DEBUG> Alert done! timer: %ld", self.alertTimer);
                    self.alertTimer = -1;
                    self.listenStatusLabel.text = @"Listening...";
                    self.listenButtonLabel.text = @"😇";
                    [[self view] setBackgroundColor:BKG_LISTENING];
                } else if (self.alertTimer > 0) {
                    // Word alert active.
                    //NSLog(@"DEBUG> Alert timer: %ld", self.alertTimer);
                    --self.alertTimer;

                    // ???fade bkg color back to normal ???
                } else {
                    // Alert inactive.
                }

                self.listenTimerLabel.text = [NSString stringWithFormat:@"%u:%02u", minutes, seconds];
            }
        }
        break;
        
        default:
            NSAssert1(FALSE, @"ERROR> Invalid listen.state: %ld", (long)self.listenState);
    }
    
    // Recursion.
    [self performSelector:@selector(updateTimer) withObject:self afterDelay:0.1];
    
    return;
}

- (IBAction)handleButtonTap:(id)sender {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    if (self.listenState == LISTEN_READY) {
        // Inactive -> Start
        NSLog(@"DEBUG> User has tapped start button ==> START LISTENING!");

        // Make sure current duration setting is being used.
        self.duration = appDelegate.sessionDuration + LISTEN_COUNTDOWN; // ??? This needs to be read from UserDefaults. ???
        NSLog(@"DEBUG> Session duration: %f", appDelegate.sessionDuration);

        // Reset word counts.
        NSAssert2([self.words count] == [self.wordCount count], @"DEBUG> handleButtonTap *** ERROR *** Blacklist[]: %lu entries, but word count[]: %lu entries", (unsigned long)[self.words count], (unsigned long)[self.wordCount count]);
        for (int i = 0; i < [self.wordCount count]; i++) {
            self.wordCount[i] = @(0);
        }
        self.listenButtonLabel.text = [NSString stringWithFormat:@"%d", LISTEN_COUNTDOWN];      // Start n-sec countdown
        
        [self.listenButton setEnabled:FALSE];               // Disable button
        self.listenState = LISTEN_PENDING;                  // Countdown to listening

        self.startTime = [NSDate timeIntervalSinceReferenceDate];
        self.countdownCounter = 0;
        
        [self updateTimer];
    } else if (self.listenState == LISTEN_PENDING) {
        // Waiting for OE to stop.
        NSLog(@"DEBUG> ***ERROR***  Ignoring tap: User has tapped the timer button while its already running, BUT app thinks we already tapped and we're waiting for OE to start... WAITING...");
    } else if (self.listenState == LISTEN_REQUESTED) {
        // Pause?
        NSLog(@"DEBUG> ***ERROR***  Ignoring tap: User has tapped the timer button while its already started.... We're waiting for OE to confirm the start listening.");
    } else if (self.listenState == LISTEN_LISTENING) {
        // Pause?
        NSLog(@"DEBUG> ***WARNING***  Ignoring tap: User has tapped the timer button while its already listening.... Add a PAUSE feature?");
    } else if (self.listenState == LISTEN_STOPPING) {
        // Waiting for OE to stop.
        NSLog(@"DEBUG> ***ERROR***  Ignoring tap: User has tapped the timer button, BUT the app has just requested OE to stop listening... we are currently waiting for OE to confirm the stop.");
    }
}

- (IBAction)handleDurationTapped:(id)sender {
    NSLog(@"DEBUG> User has tapped CHANGE DURATION button... TBD...");
    
}

- (void)closeSessionOver:(SessionOverController *)sessionOverController {
    NSLog(@"ListenControler::closeSessionOver: Session done button tapped.");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // Create a session record and save to UserDefaults.
    SessionSummary *session = [[SessionSummary alloc] init];
    session.date = [NSDate date];
    session.duration = appDelegate.sessionDuration;
    for (int i = 0; i < [self.words count]; i++) {
        WordSummary *wordSummary = [[WordSummary alloc] init];
        wordSummary.word = self.words[i];
        wordSummary.count = [self.wordCount[i] intValue];
        [session.sessionInfo addObject:wordSummary];
    }
    
    [self.sessionHistory addObject:session];    // add this new session to history
    
    [self saveSession];                         // save to UserDefaults. ??? Either save on Parse or other external db, or limit to fixed length array. ???
    
    // Clear session data.
    for (int i = 0; i < [self.wordCount count]; i++) {
        // Reset.
        if ([(NSNumber *)self.wordCount[i] intValue] > 0) {
            self.wordCount[i] = @(0);
        }
    }
    
    // Dismiss the modal.
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];

    return;
}

-(void) saveSession
{
    // ??? Consider truncating the sessions array to prevent runaway size. ???
    // ??? Or save to a database instead (e.g. Parse). ???
    
    // Save to User Defaults (local).
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSData *sessionData = [NSKeyedArchiver archivedDataWithRootObject:self.sessionHistory];
    [defaults setObject:sessionData forKey:@"Bleeper-history"];
    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    
    [defaults setObject:version forKey:@"Bleeper-version"];
    [defaults setObject:build forKey:@"Bleeper-build"];
    
    [defaults synchronize];
    
    return;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"DEBUG> *************** MEMORY WARNING received! ***************");

}

- (void) pocketsphinxDidReceiveHypothesis:(NSString *)hypothesis recognitionScore:(NSString *)recognitionScore utteranceID:(NSString *)utteranceID {
    NSLog(@"DEBUG> The received hypothesis is %@ with a score of %@ and an ID of %@", hypothesis, recognitionScore, utteranceID);
    
    // First, separate into words (if needed)
    NSCharacterSet *whitespaces = [NSCharacterSet whitespaceCharacterSet];
    NSPredicate *noEmptyStrings = [NSPredicate predicateWithFormat:@"SELF != ''"];
    
    NSArray *separatedWords = [hypothesis componentsSeparatedByCharactersInSet:whitespaces];
    NSArray *hypothesisWords = [separatedWords filteredArrayUsingPredicate:noEmptyStrings];
    
    //AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);  ???????????  AUDIO/VIBRATE DOES NOT SEEM TO WORK WHEN CALLED FORM HERE  ....  WEIRD !!!!!!    ???????????

    // Update view.
    [[self view] setBackgroundColor:BKG_WORD_DETECTED];
    self.listenStatusLabel.text = [NSString stringWithFormat:@"`%@'", hypothesis];
    self.listenButtonLabel.text = @"👿";
    
    // Match words to blacklist.
    int i, j;
    for (i = 0; i < [hypothesisWords count]; ++i) {
        for (j = 0; j < [self.words count]; ++j) {
            if ([(NSString *)hypothesisWords[i] isEqualToString:self.words[j]]) {
                // Match!
                ++self.listenScore;
                self.wordCount[j] = @([self.wordCount[j] intValue] + 1);
                break;
            }
        }
        NSAssert1(j < [self.words count], @"FATAL ERROR: Hypothesis `%@' returned from PocketSphinx(OpenEars) is not found in the user's blacklist!", hypothesis);
    }
    
    // Start alert timer for word.
    self.alertTimer = 10;   // ticks
 
    // Update word count display.
    self.listenScoreLabel.text = [NSString stringWithFormat:@"Count: %ld", (long) self.listenScore];
    
    return;
}

- (void) pocketsphinxDidStartListening {
    // Listening has started!
    NSLog(@"Pocketsphinx is now listening.");
    
    [self.listenActivityIndicator stopAnimating];

    self.listenButtonLabel.text = @"😇";
    self.listenStatusLabel.text = @"Listening...";
    
    // Initialize word counter for each word in blacklist.
    NSAssert2([self.words count] == [self.wordCount count], @"DEBUG> pocketsphinxDidStartListening *** ERROR *** Blacklist[]: %lu entries, but word count[]: %lu entries", (unsigned long)[self.words count], (unsigned long)[self.wordCount count]);
    
    for (int i = 0; i < [self.wordCount count]; ++i) {
        self.wordCount[i] = [NSNumber numberWithInt:0];
    }

    self.listenState = LISTEN_LISTENING;
    
    return;
}

- (void) pocketsphinxDidDetectSpeech {
    NSLog(@"Pocketsphinx has detected speech.");
}

- (void) pocketsphinxDidDetectFinishedSpeech {
    NSLog(@"Pocketsphinx has detected a period of silence, concluding an utterance.");
}

- (void) pocketsphinxDidStopListening {
    NSLog(@"DEBUG> Pocketsphinx has stopped listening.");
    
    [self.listenActivityIndicator stopAnimating];

    [self.listenButton setEnabled:TRUE];
    [self.listenButton setSelected:FALSE];
    [self.listenButton setHighlighted:FALSE];

    self.listenState = LISTEN_READY;

    return;
}

- (void) pocketsphinxDidSuspendRecognition {
    NSLog(@"Pocketsphinx has suspended recognition.");
}

- (void) pocketsphinxDidResumeRecognition {
    NSLog(@"Pocketsphinx has resumed recognition.");
}

- (void) pocketsphinxDidChangeLanguageModelToFile:(NSString *)newLanguageModelPathAsString andDictionary:(NSString *)newDictionaryPathAsString {
    NSLog(@"Pocketsphinx is now using the following language model: \n%@ and the following dictionary: %@",newLanguageModelPathAsString,newDictionaryPathAsString);
}

- (void) pocketSphinxContinuousSetupDidFailWithReason:(NSString *)reasonForFailure {
    NSLog(@"Listening setup wasn't successful and returned the failure reason: %@", reasonForFailure);
}

- (void) pocketSphinxContinuousTeardownDidFailWithReason:(NSString *)reasonForFailure {
    NSLog(@"Listening teardown wasn't successful and returned the failure reason: %@", reasonForFailure);
}

- (void) testRecognitionCompleted {
    NSLog(@"A test file that was submitted for recognition is now complete.");
}

// Extended delegates for RapidEars
- (void) rapidEarsDidReceiveLiveSpeechHypothesis:(NSString *)hypothesis recognitionScore:(NSString *)recognitionScore {
    NSLog(@"RapidEars> Received hypothesis: %@, with a score of %@", hypothesis, recognitionScore);
    
    // First, separate into words (if needed)
    NSCharacterSet *whitespaces = [NSCharacterSet whitespaceCharacterSet];
    NSPredicate *noEmptyStrings = [NSPredicate predicateWithFormat:@"SELF != ''"];
    
    NSArray *separatedWords = [hypothesis componentsSeparatedByCharactersInSet:whitespaces];
    NSArray *hypothesisWords = [separatedWords filteredArrayUsingPredicate:noEmptyStrings];
    
    //AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);  ???????????  AUDIO/VIBRATE DOES NOT SEEM TO WORK WHEN CALLED FORM HERE  ....  WEIRD !!!!!!    ???????????
    
    // Update view.
    [[self view] setBackgroundColor:BKG_WORD_DETECTED];
    self.listenStatusLabel.text = [NSString stringWithFormat:@"`%@'", hypothesis];
    self.listenButtonLabel.text = @"👿";
    
    // Match words to blacklist.
    int i, j;
    for (i = 0; i < [hypothesisWords count]; ++i) {
        for (j = 0; j < [self.words count]; ++j) {
            if ([(NSString *)hypothesisWords[i] isEqualToString:self.words[j]]) {
                // Match!
                ++self.listenScore;
                self.wordCount[j] = @([self.wordCount[j] intValue] + 1);
                break;
            }
        }
        NSAssert1(j < [self.words count], @"RapidEars> FATAL ERROR: Hypothesis `%@' returned from PocketSphinx(OpenEars) is not found in the user's blacklist!", hypothesis);
    }
    
    // Start alert timer for word.
    self.alertTimer = 10;   // ticks
    
    // Update word count display.
    self.listenScoreLabel.text = [NSString stringWithFormat:@"Count: %ld", (long) self.listenScore];
    
    return;

}

- (void) rapidEarsDidReceiveFinishedSpeechHypothesis:(NSString *)hypothesis recognitionScore:(NSString *)recognitionScore {
    NSLog(@"rapidEarsDidReceiveFinishedSpeechHypothesis: %@",hypothesis);
    
}

@end
