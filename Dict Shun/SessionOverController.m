//
//  SessionOverController.m
//  Bleeper
//
//  Created by Rikk Carey on 4/22/15.
//  Copyright (c) 2015 Dictionary.com. All rights reserved.
//

#import "SessionOverController.h"

@interface SessionOverController ()

@end

@implementation SessionOverController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Add the Cancel button
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];
    
    self.navigationItem.rightBarButtonItem = doneBtn;
}

-(void)viewWillAppear:(BOOL)animated {
    NSLog(@"DEBUG> SessionOverController::viewWillAppear: Reload the data...");
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)done:(id)sender {
    // Session `Done' button tapped.
    NSLog(@"DEBUG> Session done tapped!");
    
    // Invoke delegate to pass control to parent to dismiss.
    // note: I think its necessary to cast the delegate as a strong.
    id<SessionOverDelegate> strongDelegate = self.delegate;
    [strongDelegate closeSessionOver:self];

    return;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    /*
    int rowCount = 0;
    
    for (int i = 0; i < [self.wordCount count]; i++) {
        // Display words that were detected >= 1 time.
        if ([(NSNumber *)self.wordCount[i] intValue] > 0) {
            ++rowCount;
        }
    }
    
    NSLog(@"DEBUG> Session over, # rows: %d", rowCount);
    
     return rowCount;*/
     return [self.wordCount count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Configure the cell...
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SessionOverCell"];
    
    /*
    // Get existing cell values.
    NSString *word;
    int rowIndex = 0;
    for (int i = 0; i < [self.wordCount count]; i++) {
        // Only display words that were detected >= 1 time.
        if ([(NSNumber *)self.wordCount[i] intValue] > 0) {
            if (rowIndex == indexPath.row) {
                word = self.words[indexPath.row];
                break;
            } else {
                ++rowIndex;
            }
        }
    }*/
    
    NSString *word = self.words[indexPath.row];

    cell.textLabel.text = [NSString stringWithFormat:@"😈 `%@' detected %d times", word, [((NSNumber *) self.wordCount[indexPath.row]) intValue]];
    
    NSLog(@"DEBUG> Session over tableView cellForRowAtIndexPath[%zd] = %@.  ", indexPath.row, word);
    
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
