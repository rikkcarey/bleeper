//
//  ListenController.h
//  Dict Shun
//
//  Created by Rikk Carey on 3/31/15.
//  Copyright (c) 2015 Dictionary.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <OpenEars/OEEventsObserver.h>
#import <RapidEarsDemo/OEEventsObserver+RapidEars.h>    // If using Rapid Ears (with Open Ears)
#import "SessionOverController.h"

@interface ListenController : UIViewController <OEEventsObserverDelegate, SessionOverDelegate>

// Data model
@property (strong, nonatomic)        NSMutableArray   * words;
@property (strong, nonatomic)        NSMutableArray   * wordCount;
@property (nonatomic, strong)        NSMutableArray   * sessionHistory;

// UI
@property (weak, nonatomic) IBOutlet UIButton         * listenButton;
@property (weak, nonatomic) IBOutlet UILabel          * listenButtonLabel;
@property (weak, nonatomic) IBOutlet UILabel          * listenStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel          * listenTimerLabel;
@property (weak, nonatomic) IBOutlet UILabel          * listenScoreLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView * listenActivityIndicator;

@property (nonatomic)                NSInteger          listenState;
@property (nonatomic)                NSInteger          listenScore;
@property (nonatomic)                NSTimeInterval     startTime;
@property (nonatomic)                float              duration;
@property (nonatomic)                int                countdownCounter;
@property (nonatomic, strong)        NSTimer          * timer;
@property (nonatomic)                NSInteger          alertTimer;

// Speech recognition
@property (strong, nonatomic)        OEEventsObserver * openEarsEventsObserver;
@property (strong, nonatomic)        NSString         * lmPath;
@property (strong, nonatomic)        NSString         * dicPath;

@property (nonatomic, strong) SessionOverController   * sessionOverController;

@end

