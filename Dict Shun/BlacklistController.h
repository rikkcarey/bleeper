//
//  BlacklistController.h
//  Dict Shun
//
//  Created by Rikk Carey on 4/5/15.
//  Copyright (c) 2015 Dictionary.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlackwordEntryController.h"


#pragma BlacklistController

@interface BlacklistController : UITableViewController

@property (nonatomic, strong) NSMutableArray            * words;
@property (nonatomic, strong) NSMutableArray            * wordCount;
@property (nonatomic, strong) NSMutableArray            * sessionHistory;

@property (nonatomic, strong) BlackwordEntryController  * wordEntryController;

@property (strong, nonatomic) OELanguageModelGenerator * lmGenerator;

@end
