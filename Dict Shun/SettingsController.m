//
//  SettingsController.m
//  Bleeper
//
//  Created by Rikk Carey on 4/29/15.
//  Copyright (c) 2015 Dictionary.com. All rights reserved.
//

#import "SettingsController.h"
#import "AppDelegate.h"

@interface SettingsController ()

@end

@implementation SettingsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.VADThresholdControl.selectedSegmentIndex     = appDelegate.VADThresholdSetting;
    self.secondsOfSilenceControl.selectedSegmentIndex = appDelegate.secondsOfSilenceSetting;
    self.sessionDurationControl.selectedSegmentIndex  = appDelegate.sessionDurationSetting;
    self.rapidEarsSwitch.on  = appDelegate.rapidEarsEnabled;

    return;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.

    if (section == 0) {
        // App settings
        return 1;
    } else {
        // Speech recognition settings
        return 4;
    }
    
}

- (IBAction)setSessionDuration:(UISegmentedControl *)sender {
    NSLog(@"DEBUG> setSessionDuration tapped!");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    appDelegate.sessionDurationSetting = (int) sender.selectedSegmentIndex;
    
    // Write updated value to UserDefaults.
    [defaults setInteger:appDelegate.sessionDurationSetting forKey:@"Bleeper-setting-session-duration"];
    [defaults synchronize];
    
    return;
}

- (IBAction)setVADThreshold:(UISegmentedControl *)sender {
    // VADThreshold default=1.5, range: 0.5-5.0, max=3.0  Recommended by Halle of OpenEars project
    NSLog(@"DEBUG> setVADThreshold tapped!");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    appDelegate.VADThresholdSetting = (int) sender.selectedSegmentIndex;

    // Write updated value to UserDefaults.
    [defaults setInteger:appDelegate.VADThresholdSetting forKey:@"Bleeper-setting-VADThreshold"];
    [defaults synchronize];
    
    return;
}
- (IBAction)setSecondsOfSilence:(UISegmentedControl *)sender {
    // SecondsOfSilence default=0.7, min=0.3  Recommended by Halle of OpenEars project
    NSLog(@"DEBUG> setSecondsOfSilence tapped!");

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    appDelegate.secondsOfSilenceSetting = (int) sender.selectedSegmentIndex;
    
    // Write updated value to UserDefaults.
    [defaults setInteger:appDelegate.secondsOfSilenceSetting forKey:@"Bleeper-setting-secondOfSilence"];
    [defaults synchronize];
    
    return;
}

- (IBAction)tappedResetSpeechRecognitionSettings:(id)sender {
    NSLog(@"DEBUG> setResetSpeechRecognitionSettings tapped!");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    appDelegate.secondsOfSilenceSetting = (int) SETTING_SECONDS_OF_SILENCE_DEFAULT;
    appDelegate.VADThresholdSetting = (int) SETTING_VAD_THRESHOLD_DEFAULT;
    
    self.VADThresholdControl.selectedSegmentIndex     = appDelegate.VADThresholdSetting;
    self.secondsOfSilenceControl.selectedSegmentIndex = appDelegate.secondsOfSilenceSetting;
    [self.tableView reloadData];
    
    // Write updated value to UserDefaults.
    [defaults setInteger:appDelegate.VADThresholdSetting forKey:@"Bleeper-setting-VADThreshold"];
    [defaults setInteger:appDelegate.secondsOfSilenceSetting forKey:@"Bleeper-setting-secondOfSilence"];
    
    [defaults synchronize];
    
    return;
}
- (IBAction)tappedRapidEarsSwitch:(UISwitch *)sender {
    NSLog(@"DEBUG> RapidEars switch tapped!");

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    appDelegate.rapidEarsEnabled = sender.on;
    [self.tableView reloadData];
    
    // Write updated value to UserDefaults.
    [defaults setBool:appDelegate.rapidEarsEnabled forKey:@"Bleeper-setting-rapidEars"];

    [defaults synchronize];
    
    return;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
