//
//  BlacklistController.m
//  Dict Shun
//
//  Created by Rikk Carey on 4/5/15.
//  Copyright (c) 2015 Dictionary.com. All rights reserved.
//

#import "BlacklistController.h"
#import "BlackwordEntryController.h"
#import <OpenEars/OELanguageModelGenerator.h>
#import <OpenEars/OEAcousticModel.h>

@interface BlacklistController ()

@end

@implementation BlacklistController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    if ([self.words count] < 10) {
        UIBarButtonItem *addBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addBlacklistWord:)];
        self.navigationItem.leftBarButtonItem = addBtn;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    NSLog(@"DEBUG> SessionOverController::viewWillAppear: Reload the data...");
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.words count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Configure the cell...
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BlacklistCell"];
    
    // Get existing cell.
    NSString *word = (self.words)[indexPath.row];
    cell.textLabel.text = word;
    //cell.detailTextLabel.text = word;
    NSLog(@"DEBUG> Blacklist tableView cellForRowAtIndexPath[%zd] = %@.  ", indexPath.row, word);

    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)addBlacklistWord:sender {
    // User has tapped "+" button ==>  "Add word" modal
    if (self.wordEntryController == nil) {
        UIStoryboard *storyboard = self.storyboard;
        self.wordEntryController = [storyboard instantiateViewControllerWithIdentifier:@"BlackwordEntryView"];
        self.wordEntryController.words = self.words;
        self.wordEntryController.wordCount = self.wordCount;
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.wordEntryController];
    
    [[self navigationController] presentViewController:navigationController animated:YES completion:nil];
}

 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
     if (editingStyle == UITableViewCellEditingStyleDelete) {
         // Remove word from black list.
         NSString *word = (self.words)[indexPath.row];
         NSLog(@"DEBUG> [tableView commitEditingStyle] Deleting blacklist row: %zd, word: %@....", indexPath.row, word);
         
         // Delete the row from the data source
         [self.words removeObjectAtIndex:indexPath.row];
         [self.wordCount removeObjectAtIndex:indexPath.row];

         [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
         NSLog(@"DEBUG> [tableView commitEditingStyle] Successfully deleted: %@", word);
         
         // Save blacklist to User Defaults (local).
         NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
         
         NSData *blacklistData = [NSKeyedArchiver archivedDataWithRootObject:self.words];
         [defaults setObject:blacklistData forKey:@"Bleeper-blacklist-words"];
         
         [self saveVersionBuild:defaults];
         [defaults synchronize];
         
         // Regenerate language model.
         NSError *err = [self.lmGenerator generateLanguageModelFromArray:_words withFilesNamed:@"MyLanguageModelFiles" forAcousticModelAtPath:[OEAcousticModel pathToModel:@"AcousticModelEnglish"]];
         
         if (err == nil) {
             NSLog(@"DEBUG> Language model SUCCESSFULLY generated, for keywords: %@!", _words);
         } else {
             NSLog(@"DEBUG> Error generating lanaguage model for keywords list: %@",[err localizedDescription]);
         }
     }
 }

-(void) saveVersionBuild:(NSUserDefaults *)defaults
{
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    
    [defaults setObject:version forKey:@"Bleeper-version"];
    [defaults setObject:build forKey:@"Bleeper-build"];
    
    [defaults synchronize];
    
    return;
}

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
