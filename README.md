# README #

### What is this repository for? ###

* Prototype app to test speech recognition for Dictionary.com. Originally wanted to build as an Apple Watch app, but discovered that WatchKit does not enable microphone access (besides indirectly via Siri). So, we decided to build an iPhone app to test speech recognition for this use case.
* Concept: User creates a list of "black words" that they want to stop saying. The App listens to user's speech during "listening sessions" and detects when black words (or phrases) are spoken, alerts the user, and keeps a record.
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Clone the tree
* Configuration: Install OpenEars and RapidEars frameworks as described here: http://www.politepix.com/openears/ and http://www.politepix.com/rapidears/
* NOTE: RapidEars requires -ObjC linker flag due to use of category extension.
* Dependencies: Uses OpenEars/Pocketsphinx + RapidEars plugin open source speech recognition framework
* Database configuration: none
* How to run tests: none yet
* Deployment instructions: none :)

### Contribution guidelines ###

* Writing tests TBD
* Code review TBD
* Other guidelines TBD